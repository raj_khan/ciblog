-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2018 at 11:24 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ciblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_at`) VALUES
(1, 1, 'Business', '2018-02-23 09:05:33'),
(2, 2, 'Technology', '2018-02-23 09:05:33');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `name`, `email`, `body`, `created_at`) VALUES
(1, 7, 'Emrul', 'bsd@gmail.com', 'Great Post', '2018-03-01 07:45:13'),
(2, 9, 'Eva Rokya', 'evarokya1@gmail.com', 'Awesome\r\n', '2018-03-05 05:20:57'),
(3, 9, 'Emrul', 'emrul@gmail.com', 'WOW', '2018-03-05 05:23:44');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `user_id`, `title`, `slug`, `body`, `post_image`, `created_at`) VALUES
(7, 1, 1, 'Test Post', 'Test-Post', '<p>This is Test Post</p>\r\n', 'images.jpg', '2018-02-23 09:51:10'),
(8, 1, 1, 'Post No Image', 'Post-No-Image', '<p>this is post with no image</p>\r\n', 'noimage.jpg', '2018-02-23 09:57:27'),
(9, 1, 1, 'title', 'title', '<p>body</p>\r\n', 'noimage.jpg', '2018-02-25 08:18:46'),
(10, 1, 1, 'Blog Post Four', 'Blog-Post-Four', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat consectetur dolor tempor tempor. Aliquam pretium purus in leo semper, ac sodales leo imperdiet. Duis consequat pretium lectus ac placerat. Cras vel malesuada purus, vel aliquet velit. Phasellus in fringilla lorem, sed molestie odio. Aenean sollicitudin est laoreet arcu mattis elementum. In hac habitasse platea dictumst. Aenean magna dui, vestibulum eget egestas ut, tincidunt sed augue. Donec in eros non orci congue suscipit ac vitae erat. Aenean nec nunc blandit ligula volutpat cursus non quis nisi. Nulla tristique, risus at consectetur congue, felis massa pellentesque diam, sed bibendum odio enim interdum elit. Cras pretium porta mauris nec fringilla. Mauris volutpat, enim a elementum interdum, lacus diam semper sem, eget consequat justo est et nibh. Pellentesque non quam at dui pretium facilisis varius eleifend magna. Curabitur egestas luctus condimentum. Pellentesque dapibus rutrum mauris vitae lacinia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a luctus felis. Nullam facilisis, mauris sed venenatis condimentum, augue nisl mollis risus, et suscipit magna est sed dolor. Sed dignissim feugiat sapien auctor porta. Quisque non vehicula mi, in feugiat tortor. Maecenas vulputate aliquet interdum. Aliquam pellentesque nunc nec purus iaculis laoreet. Nulla at neque sit amet dui placerat tempor vitae id neque. Morbi sit amet purus ac purus ullamcorper fermentum eget consequat nisi.</p>\r\n', 'noimage.jpg', '2018-03-05 07:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `zipcode`, `email`, `username`, `password`, `register_date`) VALUES
(1, 'Raj Khan', '1212', 'rajbsd.com@gmail.com', 'raj', '202cb962ac59075b964b07152d234b70', '2018-03-01 09:36:38'),
(2, 'Eva Rokya', '1235', 'evarokya1@gmail.com', 'eva', '202cb962ac59075b964b07152d234b70', '2018-03-05 05:19:15'),
(3, 'Emrul Bhai', '1258', 'emrul@gmail.com', 'emrul', '202cb962ac59075b964b07152d234b70', '2018-03-05 05:23:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
