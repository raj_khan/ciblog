<h2><?php echo $title; ?></h2>

<?php echo validation_errors(); ?>


<?php echo form_open_multipart('posts/create'); ?>
<div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" aria-describedby="emailHelp" placeholder="Add Title">
</div>


<div class="form-group">
    <label>Body</label>
    <textarea id="editor1" class="form-control" name="body" placeholder="Add Body"></textarea>
</div>


<!--Categgories-->
<div class="form-group">
    <label for="">Category</label>
    <select name="category_id" id="" class="form-control">
        <?php foreach ($categories as $category): ?>

            <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>

        <?php endforeach; ?>
    </select>
</div>


<!--File Upload-->
<div class="form-group">
    <label for="">Upload Image</label>
    <input type="file" name="userfile" size="20">
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>